import { Container, Row, Col } from 'react-bootstrap';

export default function Footer() {
  return (
    <footer className="bg-light py-4">
      <Container>
        <Row>
          <Col md={4}>
            <h4>About Us</h4>
            <p>
              Jordan Kicks is the ultimate sneaker store for all your needs.
              Our collection features the latest styles and designs from
              Jordan, ensuring that you step up your game every time.
            </p>
          </Col>
          <Col md={4}>
            <h4>Connect With Us</h4>
            <p>
              <i className="bi bi-geo-alt"></i> 123 Cubao, Quezon City
            </p>
            <p>
              <i className="bi bi-phone"></i> (02) 87000
            </p>
            <p>
              <i className="bi bi-envelope"></i> info@jordankicks.com
            </p>
          </Col>
          <Col md={4}>
            <h4>Stay Updated</h4>
            <p>
              Sign up for our newsletter and be the first to know about new
              releases, special offers, and exclusive events.
            </p>
            <form>
              <div className="form-group">
                <input
                  type="email"
                  className="form-control"
                  placeholder="Enter your email"
                />
              </div>
              <button type="submit" className="btn btn-primary">
                Subscribe
              </button>
            </form>
          </Col>
        </Row>
      </Container>
      <div className="text-center bg-dark text-white py-2">
        <p>&copy; 2023 Jordan Kicks. All rights reserved.</p>
      </div>
    </footer>
  );
}
