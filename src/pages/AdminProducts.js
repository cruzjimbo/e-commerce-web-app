import { useEffect, useState } from "react";
import Dashboard from "../components/Dashboard";

export default function Products() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`https://capstone2-cruz.onrender.com/products/allProducts`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setProducts(
          data.map((product) => {
            return <Dashboard key={product._id} product={product} />;
          })
        );
      });
  }, []);

  return (
      <div className="products-container">
      { products }
    </div>
    )
}
